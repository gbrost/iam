# Identity and Access Management

## Presentation of working group

The objective of the Identity and Access Management working group is to synthesize all the contributions (GXFS-EN
 GXFS-DE
 Gaia-X
 external) on the subject into a coherent
 detailed and usable specification document for software implementations.

The mission of this working group is described in the [Identity and Access Management Mission Document](https://gitlab.com/gaia-x/technical-committee/operational-handbook/-/blob/master/docs/mission_documents/technical-committee_architecture_identity-and-access-management.yaml)

## Meetings schedule

* Every Tuesday at 5pm on [Teams](https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZGY4NzU0MjUtZDI3YS00OTkxLWJhOTQtYTQxZmE2N2NkNzhj%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22b4f707b0-251e-452e-859a-40e139c49613%22%7d)

## Permanent participants

* Antonio La Marra <antoniolamarra@security-forge.com>
* boris.lingl@datev.de
* bryn.robinson-morgan@mastercard.com
* Kai Meinke <kai@delta-dao.com>
* Martin Matthiesen <martin.matthiesen@csc.fi>
* ohad.arnon@dell.com
* Olivier Caudron <olivier.caudron@ovhcloud.com>
* olivier senot <olivier.senot@docaposte.fr>
* perrin.grandne@education.gouv.fr
* peter.koen@microsoft.com
* wg-identity-and-access-management@list.gaia-x.eu

## Meeting Minutes 

All minutes are available on [minutes directory](minutes/).

## Specification

The (work in progress) specification document is available [here](spec/L01_IDM_AA/idm_aa.md).

The redactional work is provided by participants on dedicated branches and then submitted for merge request.

## Current working group objectives

* First release of specifications by June 2022